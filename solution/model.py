import torch
import torch.nn as nn
import torch.nn.functional as F


class MultiLayerPerceptron(nn.Module):
    r"""Parameters
    ----------
    input_dims : int
        Input feature dimensions.
    num_classes : int
        Number of output classes.
    layer_params : list
        List of the feature size for each layer.
    """

    def __init__(self, input_dims, num_classes,
                 layer_params=(256,64,16),
                 **kwargs):

        super(MultiLayerPerceptron, self).__init__(**kwargs)
        channels = [input_dims] + list(layer_params) + [num_classes]
        layers = []
        for i in range(len(channels) - 1):
            layers.append(nn.Sequential(nn.Linear(channels[i], channels[i + 1]),
                                        nn.ReLU()))
        self.mlp = nn.Sequential(*layers)
        self.loss = nn.CrossEntropyLoss()

    def forward(self, x):
        # x: the feature vector initally read from the data structure, in dimension (N, C, P)
        x = x.flatten(start_dim=1) # (N, L), where L = C * P
        return self.mlp(x)

    def predict(self,x):
        pred = F.softmax(self.forward(x))
        ans = []
        for t in pred:
            if t[0] > t[1]:
                ans.append(1)
            else:
                ans.append(0)
        return torch.tensor(ans)

class Model:
    def __init__(self):
        self.model = MultiLayerPerceptron(input_dims=4*30, num_classes=2)
        self.loss = nn.CrossEntropyLoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=0.001)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


    def fit(self, X_train, y_train):
        self.model.to(self.device)
        self.loss.to(self.device)
        for epoch in range(1,10+1):
            X_train_dev = torch.from_numpy(X_train)
            y_train_dev = torch.from_numpy(y_train).long()
            self.optimizer.zero_grad()
            pred = self.model.forward(X_train_dev)
            loss = self.loss(pred, y_train_dev)
            loss.backward()
            self.optimizer.step()
            print(f'epoch {epoch} loss {loss.item()}')

    def predict(self, X_test):
        X_test_dev = torch.from_numpy(X_test)
        return self.model.predict(X_test_dev)
