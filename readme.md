# CMS ML Benchmark system

1. Based on CodaBench (CodaLab V2) [link](https://www.codabench.org/competitions/428/?secret_key=37ce92d2-d9dc-4d9d-a8c2-d29b78b6f445)
    - A codalab V1 based version can be found in the v1 branch, but harder to use & debug
2. Need to run
```
zip -o -r ../test.zip .
```
in the base directory to create a submission tarball.
