import json
import os
import sys
import time

import numpy as np
import pandas as pd

input_dir = '/app/input_data/'
output_dir = '/app/output/'
program_dir = '/app/program'
submission_dir = '/app/ingested_program'

sys.path.append(program_dir)
sys.path.append(submission_dir)

def get_PF_feature(name,n_pad=30):
    with open(os.path.join(input_dir,name),"r") as f:
        input_feathers_origin =  json.load(f)
    branches = []
    dfs = []
    for key1 in input_feathers_origin:
        input_df = pd.read_json(input_feathers_origin[key1])
        new_df = {}
        for key2 in input_df.columns:
            if not (key2 in branches):
                branches.append(key2)
            if len(input_df[key2]) > n_pad:
                new_df[key2] = np.array(input_df[key2])[:n_pad]
            else:
                new_df[key2] = np.hstack([np.array(input_df[key2]), np.zeros(n_pad - len(input_df[key2]))])
        dfs.append(new_df)

    X = [ np.hstack([i[key].copy() for key in branches]) for i in dfs]
    X = np.vstack(X)
    return X.astype("float32")
    # input_feathers_dict = {}
    # print(input_feathers_origin)
    # for key in input_feathers_origin:
    #     input_feathers_dict[key] = np.array(input_feathers_origin[key]).pad(n_pad,clip=True).fillna(0)
    # return pd.DataFrame(input_feathers_dict)

def get_training_data():
    X_train = get_PF_feature('test_input.json')
    y_train = np.genfromtxt(os.path.join(input_dir, 'training_label')).astype("float32")
    return X_train, y_train


def get_prediction_data():
    return get_PF_feature('test_input.json')


def main():
    from model import Model
    print('Reading Data')
    X_train, y_train = get_training_data()
    X_test = get_prediction_data()
    print('-' * 10)
    print('Starting')
    start = time.time()
    m = Model()
    print('-' * 10)
    print('Training Model')
    m.fit(X_train, y_train)
    print('-' * 10)
    print('Running Prediction')
    prediction = m.predict(X_test)
    duration = time.time() - start
    print('-' * 10)
    print(f'Completed Prediction. Total duration: {duration}')
    np.savetxt(os.path.join(output_dir, 'prediction'), prediction)
    with open(os.path.join(output_dir, 'metadata.json'), 'w+') as f:
        json.dump({'duration': duration}, f)
    print()
    print('Ingestion Program finished. Moving on to scoring')


if __name__ == '__main__':
    main()
